all: build

IMAGE := umoci-node-demo
TAG := latest

.PHONY: login
login:
	skopeo login registry.gitlab.com

.PHONY: build
build:
	skopeo copy docker://node:16-slim oci:$(IMAGE):$(TAG)
	npm ci --prefix ./app
	umoci config --image $(IMAGE):$(TAG) --config.cmd "node" --config.cmd "index.js"
	umoci config --image $(IMAGE):$(TAG) --config.workingdir "/home/node/app"
	umoci insert --rootless --image $(IMAGE):$(TAG) app /home/node/app
	umoci insert --rootless --image $(IMAGE):$(TAG) app/node_modules /home/node/app/node_modules
	skopeo copy oci:$(IMAGE):$(TAG) docker://registry.gitlab.com/olof-nord/$(IMAGE):$(TAG)
