# umoci-node-demo

Demo project for a rootless GitLab CI docker build using umoci and skopeo.

[umoci](https://github.com/opencontainers/umoci)

[skopeo](https://github.com/containers/skopeo)

## Run

```shell
$ docker run -it --rm registry.gitlab.com/olof-nord/umoci-node-demo
 _______________________
< Hello from JavaScript >
 -----------------------
        \   ^__^
         \  (oO)\_______
            (__)\       )\/\
             U  ||----w |
                ||     ||
```

## Build

This requires `umoci`, `skopeo`, `nodejs`, `npm` and `make` to be installed locally.

```shell
make build
```
